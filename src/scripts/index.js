// YOUTUBE

var players = []; // Lista para armazenar todos os players de vídeo

var videoIds = ['vFae1uI3JeA', 'YoBp5AyGsDg']; // Substitua com os IDs dos vídeos

function onYouTubeIframeAPIReady() {
	videoIds.forEach(function (videoId, index) {
		var player = new YT.Player('player' + (index + 1), {
			height: '315',
			width: '560',
			videoId: videoId,
			playerVars: {
				autoplay: 0, // 0 para desativar a reprodução automática
				controls: 1, // 1 para mostrar controles do player, 0 para desativar
			},
			events: {
				onReady: onPlayerReady,
				onStateChange: onPlayerStateChange,
			},
		});
		// IDs dos vídeos que deseja incorporar

		// Inicializa um player de vídeo para cada vídeo no carrossel

		// Adiciona o player à lista de players
		players.push(player);
	});

	// Inicializa o carrossel após a configuração dos players de vídeo
}

function onPlayerStateChange(event) {
	// O estado do player mudou (por exemplo, quando o vídeo terminou de ser reproduzido)
}

function onCarouselChange(event) {
	// Esta função é chamada quando um novo item é ativado no Owl Carousel
	// Pausa todos os vídeos quando o próximo item é ativado
	players.forEach(function (player) {
		player.pauseVideo();
	});
}

function onPlayerReady(event) {
	// O player está pronto para ser controlado aqui

	// Inicializa o Owl Carousel e adiciona um ouvinte para detectar quando um novo item é ativado
	$('.ubirata .mothers .carouselVideos').owlCarousel({
		loop: false,
		navSpeed: 500,
		center: false,
		rewind: true,
		margin: 20,
		responsiveClass: true,
		nav: true,
		autoplay: false,
		autoWidth: false,
		navContainerClass: 'navWhite owl-nav',
		onInitialized: function () {
			$('.owl-item.active')
				.find('iframe')
				.attr('src', function (i, src) {
					return src + '?autoplay=1';
				});
		},
		onChanged: onCarouselChange,
		responsive: {
			0: {
				items: 1,
			},
			800: {
				items: 1,
			},
			1024: {
				items: 1,
			},
			1950: {
				items: 1,
			},
		},
	});
}

$(document).ready(function () {
	$('.ubirata .themesMenu').click(function (e) {
		$('.ubirata').removeClass('productsActive');
		$('.ubirata').toggleClass('themesActive');
	});

	$('.ubirata .themesList a').hover(
		function () {
			$('.ubirata .themesList a').not(this).addClass('opaque');
		},
		function () {
			$('.ubirata .themesList a').removeClass('opaque');
		}
	);

	$('.ubirata .productsMenu').click(function (e) {
		$('.ubirata').removeClass('themesActive');
		$('.ubirata').toggleClass('productsActive');
	});

	$('.ubirata .hamb-background').click(function (e) {
		$('.ubirata').toggleClass('modalMobileActive');
	});

	$('.ubirata .logo-links nav').click(function (e) {
		$('.ubirata .logo-links nav a,.ubirata .logo-links nav h3 ').removeClass(
			'active'
		);
		if ($(e.target).is('h3, a')) {
			$(e.target).toggleClass('active');
		}
	});

	$('.ubirata .menuStatic .searchInput').click(function () {
		// Adicione a classe "searchModalActive" ao elemento body
		$('body').addClass('searchModalActive');
	});

	if ($(window).width() > 1024) {
		$(document).on('click', function (event) {
			if ($('.ubirata').hasClass('searchModalActive')) {
				if (!$(event.target).closest('.search').length) {
					// Se o clique não ocorrer dentro do modal, oculte o modal
					$('body').removeClass('searchModalActive');
				}
			}
		});
	}

	$('.ubirata .menuStatic .lupa').click(function () {
		// Adicione a classe "inputLupaActive" ao elemento body
		if ($('body').hasClass('searchModalActive')) {
			$('body').removeClass('searchModalActive');
			setTimeout(function () {
				$('body').toggleClass('inputLupaActive');
			}, 600);
		} else {
			$('body').toggleClass('inputLupaActive');
		}
	});

	$('.ubirata .menuFloated .lupa').click(function () {
		// Animação suave para rolar até o topo do body
		$('body,html').animate({ scrollTop: 0 }, 'slow');
		$('body').addClass('inputLupaActive');
	});

	$('.ubirata .modalMenuMobile .searchInput').click(function () {
		$('body').addClass('searchModalMobileActive');
		console.log('clicou');
	});

	$('.ubirata .hamb-background').click(function () {
		if ($('body').hasClass('searchModalMobileActive')) {
			$('body').removeClass('searchModalMobileActive');
			$('body').addClass('modalMobileActive');
		}
	});

	$('.ubirata .search').click(function () {
		if ($('body').hasClass('searchModalMobileActive')) {
			$('body').removeClass('searchModalMobileActive');
			$('body').removeClass('modalMobileActive');
		}
	});

	$('.ubirata .links4 .seeMore').click(function () {
		// Remove a classe 'disabled' de todos os elementos 'a' dentro do elemento 'nav'
		$(this).siblings('nav').find('.disabled').removeClass('disabled');

		// Remove o elemento com a classe '.seeMore' da tela
		$(this).remove();
	});

	$('.ubirata .otherCategoriesList-mobile').owlCarousel({
		loop: false,
		navSpeed: 500,
		center: false,
		margin: 20,
		responsiveClass: true,
		nav: true,
		dots: false,
		autoWidth: false,
		navContainerClass: 'navGreen owl-nav',
		responsive: {
			0: {
				items: 1,
				margin: 10,
			},
			300: {
				items: 2,
				margin: 10,
			},
			800: {
				items: 2,
				margin: 10,
			},
			1024: {
				items: 4,
			},
			1700: {
				items: 5,
			},
		},
	});

	var mainElement = $('main');
	if (mainElement.hasClass('age') || mainElement.hasClass('theme')) {
		function paginationCodes() {
			$('.arrowRight').click(function () {
				var activePage = $('.pagination p.active');
				var nextPage = activePage.next('p');

				if (nextPage.length > 0) {
					activePage.removeClass('active');
					nextPage.addClass('active');
				}

				// Atualiza as classes das setas
				updateArrowClasses();
				updateGridClasses();
			});

			$('.arrowLeft').click(function () {
				var activePage = $('.pagination p.active');
				var prevPage = activePage.prev('p');

				if (prevPage.length > 0) {
					activePage.removeClass('active');
					prevPage.addClass('active');
				}

				// Atualiza as classes das setas
				updateArrowClasses();
				updateGridClasses();
			});

			function updateArrowClasses() {
				var $paginationItems = $('.pagination p');
				var $arrowLeft = $('.arrowLeft');
				var $arrowRight = $('.arrowRight');

				// Remove a classe 'not-active' de ambos os elementos de seta
				$arrowLeft.removeClass('not-active');
				$arrowRight.removeClass('not-active');

				// Verifica se a classe 'active' está no primeiro ou no último item
				if ($paginationItems.first().hasClass('active')) {
					$arrowLeft.addClass('not-active');
				}

				if ($paginationItems.last().hasClass('active')) {
					$arrowRight.addClass('not-active');
				}
			}

			// Código funciona da seguinte maneira: quando o item p (botão de paginação) muda de classe ele verifica o grid que está sem a classe disabled, adiciona a ele e remove do próximo.

			function updateGridClasses() {
				var $activeGrid = $('.grid:not(.disabled)');

				if ($activeGrid.length > 0) {
					// Adiciona a classe 'disabled' ao elemento com classe 'grid' ativo
					$activeGrid.addClass('disabled');

					// Encontra o próximo elemento 'grid' após o elemento ativo e remove a classe 'disabled' dele
					var $nextGrid = $activeGrid.next('.grid');
					$nextGrid.removeClass('disabled');
					$('.ubirata .grid').masonry('layout');
				}
			}

			// Chame isso inicialmente para configurar as classes das setas com base na posição inicial
			updateArrowClasses();
		}
	}

	$(window).scroll(function () {
		if ($(window).width() <= 1024) {
			if ($(this).scrollTop() >= 30) {
				$('.ubirata').addClass('menuActive');
			} else {
				$('.ubirata').removeClass('menuActive');
			}
		} else {
			if ($(this).scrollTop() >= 108) {
				$('.ubirata').addClass('menuActive');
			} else {
				$('.ubirata').removeClass('menuActive');
			}
		}

		if ($('main').hasClass('.sustainability')) {
			var linksBellowOffset = $('.ubirata .sustainability .embed').offset().top;
			if (linksBellowOffset) {
				if ($(this).scrollTop() >= linksBellowOffset) {
					$('.ubirata').removeClass('linksActive');
				}
			}
		}
	});

	$('.ubirata .productsList').owlCarousel({
		loop: false,
		navSpeed: 500,
		margin: 20,
		responsiveClass: true,
		nav: true,
		dots: false,
		autoWidth: false,
		navContainerClass: 'navDark owl-nav',
		responsive: {
			0: {
				items: 1,
				nav: false,
			},
			800: {
				items: 1,
				nav: false,
			},
			1024: {
				items: 4,
			},
			1700: {
				items: 5,
			},
		},
	});

	$('.ubirata .otherCategoriesList').owlCarousel({
		loop: false,
		navSpeed: 500,
		center: false,
		margin: 20,
		responsiveClass: true,
		nav: true,
		dots: false,
		autoWidth: false,
		navContainerClass: 'navGreen owl-nav',
		responsive: {
			0: {
				items: 1,
				margin: 10,
			},
			800: {
				items: 2,
				margin: 10,
			},
			1024: {
				items: 4,
			},
			1700: {
				items: 5,
			},
		},
	});

	$('.ubirata .accordion').click(function (e) {
		var accordion = $(this);
		var textContent = accordion.closest('.containerAccordion').find('p');

		accordion.toggleClass('active');
		textContent.toggleClass('active');
	});

	$('.ubirata .linksList .linksAccordion').click(function (e) {
		var accordionLinks = $(this);
		var linksContent = accordionLinks.closest('div').find('.containerHide');

		accordionLinks.toggleClass('active');
		linksContent.toggleClass('active');
	});

	$('.ubirata .seeMoreContainer a').click(function (e) {
		e.preventDefault();

		var target = $(this.hash);

		if ($('main').hasClass('categories')) {
			$('html, .ubirata').animate(
				{
					scrollTop: target.offset().top - 50,
				},
				800
			);
		} else {
			$('html, .ubirata').animate(
				{
					scrollTop: target.offset().top - 170,
				},
				800
			);
		}
	});

	if ($('main').hasClass('categories')) {
		$('.ubirata .categories .carousel-products').owlCarousel({
			loop: false,
			navSpeed: 500,
			center: false,
			margin: 20,
			responsiveClass: true,
			nav: true,
			dots: false,
			autoWidth: false,
			navContainerClass: 'navGreen owl-nav',
			responsive: {
				0: {
					items: 1,
					margin: 10,
				},
				800: {
					items: 2,
					margin: 10,
				},
				1024: {
					items: 4,
				},
				1700: {
					items: 5,
				},
			},
		});
	}

	if ($('main').hasClass('sustainability')) {
		$('.ubirata .floatButton a').click(function (e) {
			e.preventDefault();

			$('.ubirata .floatButton a').removeClass('active');
			$(this).addClass('active');

			var target = $(this.hash);
			$('html, .ubirata').animate(
				{
					scrollTop: target.offset().top - 130,
				},
				800
			);
		});

		$('.ubirata .sustainability .plateContent').click(function (e) {
			e.preventDefault();

			$('.ubirata .sustainability .plateContent').removeClass('active');
			$(this).addClass('active');

			var target = $(this.hash);
			$('html, .ubirata').animate(
				{
					scrollTop: target.offset().top - 130,
				},
				800
			);
		});

		// Função para verificar se o elemento está visível na viewport
		function isInViewport(element) {
			var elementTop = element.offset().top + 300; // Somar 300 ao topo
			var elementBottom = elementTop + element.outerHeight() - 300;
			var viewportTop = $(window).scrollTop();
			var viewportBottom = viewportTop + $(window).height();
			return elementBottom > viewportTop && elementTop < viewportBottom;
		}

		// Função para atualizar a classe "active" do link
		function updateActiveLink() {
			var anySectionVisible = false; // Verifica se alguma seção está visível

			$('.ubirata .floatButton a').each(function () {
				var target = $(this.hash);
				if (isInViewport(target)) {
					anySectionVisible = true;
					$('.ubirata .floatButton a').removeClass('active');
					$(this).addClass('active');
				}
			});

			if (anySectionVisible) {
				$('.ubirata').addClass('linksActive');
			}

			// Se nenhuma seção estiver visível, remova a classe "active" de todos os links
			if (!anySectionVisible) {
				$('.ubirata .floatButton a').removeClass('active');
				$('.ubirata').removeClass('linksActive');
			}
		}

		// Adicionar manipulador de evento de rolagem
		$(window).scroll(function () {
			updateActiveLink();
		});

		// Atualizar a classe "active" ao carregar a página
		updateActiveLink();
	}

	if ($('main').hasClass('home')) {
		$('.ubirata .home .carouselKids').owlCarousel({
			loop: false,
			navSpeed: 500,
			center: false,
			margin: 20,
			responsiveClass: true,
			nav: true,
			dots: false,
			autoWidth: false,
			navContainerClass: 'navGreen owl-nav',
			responsive: {
				0: {
					items: 1,
					margin: 10,
				},
				800: {
					items: 2,
					margin: 10,
				},
				1024: {
					items: 4,
				},
				1700: {
					items: 5,
				},
			},
		});

		$('.ubirata .home .introduction').owlCarousel({
			loop: false,
			navSpeed: 500,
			center: false,
			rewind: true,
			margin: 20,
			responsiveClass: true,
			nav: true,
			dots: true,
			autoplay: true,
			autoplayTimeout: 3000,
			autoWidth: false,
			navContainerClass: 'navGreen owl-nav',
			responsive: {
				0: {
					items: 1,
					nav: false,
				},
				800: {
					items: 1,
					nav: false,
				},
				1024: {
					items: 1,
				},
				1700: {
					items: 1,
				},
			},
		});

		$('.ubirata .home .filter div p').click(function (e) {
			var filter = $(this);
			var filters = $('.ubirata .home .filter div p');
			filters.removeClass('active');
			filter.addClass('active');
			$('.grid').addClass('disabled');

			if (filter.hasClass('allPhases')) {
				$('.allFilter').removeClass('disabled');
			} else if (filter.hasClass('phase1')) {
				$('.filter1').removeClass('disabled');
			} else if (filter.hasClass('phase2')) {
				$('.filter2').removeClass('disabled');
			} else if (filter.hasClass('phase3')) {
				$('.filter3').removeClass('disabled');
			} else if (filter.hasClass('phase4')) {
				$('.filter4').removeClass('disabled');
			} else if (filter.hasClass('phase5')) {
				$('.filter5').removeClass('disabled');
			}

			$('.ubirata .grid').masonry('layout');
		});

		$('.ubirata .home .grid').masonry({
			itemSelector: '.grid-item',
			gutter: 20,
			percentPosition: true,
			stamp: '.newsLetterCard',
		});

		$('.ubirata .home .carouselActive').owlCarousel({
			loop: false,
			navSpeed: 1000,
			slideSpeed: 1000,
			center: false,
			margin: 10,
			responsiveClass: true,
			nav: true,
			dots: false,
			autoWidth: false,
			navContainerClass: 'navGreen owl-nav',
			responsive: {
				0: {
					items: 2,
				},
				800: {
					items: 2,
				},
				1024: {
					items: 4,
				},
				1700: {
					items: 5,
				},
			},
		});

		var owl = $('.introduction'); // Seletor do seu carousel
		var progressBar = $('.progress-bar');
		var play = true; // Variável para controlar se a reprodução automática está em andamento
		var playPauseBtn = $('.play-pause-btn');

		owl.on('changed.owl.carousel', function (event) {
			// Calcular a porcentagem de progresso (largura da barrinha)
			var progress = (event.item.index / event.item.count) * 100;

			// Animação da largura da barrinha de progresso
			progressBar.animate(
				{
					width: progress + '%',
				},
				800, // Duração da animação (em milissegundos)
				'linear' // Easing da animação
			);
		});

		// Botão "Pausar" ou "Continuar"
		$(playPauseBtn).click(function () {
			if (play) {
				owl.trigger('stop.owl.autoplay'); // Pausar a reprodução automática
				playPauseBtn.addClass('paused');
			} else {
				owl.trigger('play.owl.autoplay'); // Continuar a reprodução automática
				playPauseBtn.removeClass('paused');
			}
			play = !play; // Alternar entre pausar e continuar
		});
	}

	if ($('main').hasClass('products') || $('main').hasClass('home')) {
		if ($(window).width() <= 1024) {
			$('.ubirata .container-products').addClass('owl-carousel');
			$('.ubirata .container-products').addClass('owl-theme');
			$('.ubirata .container-products').addClass('carouselActive');
			$('.ubirata .carouselFilter').addClass('owl-carousel');
			$('.ubirata .carouselFilter').addClass('owl-theme');
			$('.ubirata .carouselFilter').addClass('carouselActive');
		}

		$('.ubirata .container-products2').owlCarousel({
			loop: false,
			navSpeed: 500,
			center: false,
			margin: 20,
			responsiveClass: true,
			nav: true,
			dots: false,
			autoWidth: false,
			navContainerClass: 'navGreen owl-nav',
			responsive: {
				0: {
					items: 1,
					margin: 10,
				},
				800: {
					items: 2,
					margin: 10,
				},
				1024: {
					items: 4,
				},
				1700: {
					items: 5,
				},
			},
		});

		$('.ubirata .container-products').owlCarousel({
			loop: false,
			navSpeed: 500,
			center: false,
			margin: 20,
			responsiveClass: true,
			nav: true,
			dots: false,
			autoWidth: false,
			navContainerClass: 'navGreen owl-nav',
			responsive: {
				0: {
					items: 1,
					margin: 10,
				},
				800: {
					items: 2,
					margin: 10,
				},
				1024: {
					items: 4,
				},
				1700: {
					items: 5,
				},
			},
		});

		$('.ubirata .carouselActive').owlCarousel({
			loop: false,
			navSpeed: 1000,
			slideSpeed: 1000,
			center: false,
			margin: 10,
			responsiveClass: true,
			nav: true,
			dots: false,
			autoWidth: false,
			navContainerClass: 'navGreen owl-nav',
			responsive: {
				0: {
					items: 1,
				},
				800: {
					items: 1,
				},
				1024: {
					items: 4,
				},
				1700: {
					items: 5,
				},
			},
		});

		$('.ubirata .products .nutritionalFacts .card h2').click(function (e) {
			var accordion = $(this);
			var textContent = accordion.closest('.card').find('.hide');

			accordion.toggleClass('active');
			textContent.toggleClass('active');
		});
	}

	if ($('main').hasClass('theme')) {
		paginationCodes();
		$('.ubirata .theme .grid').masonry({
			itemSelector: '.grid-item',
			gutter: 20,
			percentPosition: true,
			stamp: '.newsLetterCard',
		});

		$('.ubirata .theme #postType').SumoSelect({
			search: false,
			placeholder: 'Selecione os tipos de post',
			floatWidth: 0,
		});

		$('.ubirata .theme #postAge').SumoSelect({
			search: false,
			placeholder: 'Selecione as faixas etárias',
			floatWidth: 0,
		});

		$('.ubirata .theme #postTags').SumoSelect({
			search: false,
			placeholder: 'Selecione as tags do tema',
			floatWidth: 0,
		});

		$('.ubirata .theme #postOrder').SumoSelect({
			search: false,
			placeholder: 'Selecione as tags do tema',
			floatWidth: 0,
		});
	}

	if ($('main').hasClass('age')) {
		paginationCodes();
		$('.ubirata .age .filter p').click(function (e) {
			var filter = $(this);
			var filters = $('.ubirata .age .filter p');
			filters.removeClass('active');
			filter.addClass('active');
			varifyActiveFilter();
		});

		$('.ubirata .age .postOrder').SumoSelect({
			search: false,
			placeholder: 'Selecione as tags do tema',
			floatWidth: 0,
		});

		$('.ubirata .age .grid').masonry({
			itemSelector: '.grid-item',
			gutter: 20,
			percentPosition: true,
			stamp: '.newsLetterCard',
		});

		if ($(window).width() <= 1024) {
			$('.ubirata .age .filter').addClass('owl-carousel');
			$('.ubirata .age .filter').addClass('owl-theme');
			$('.ubirata .age .filter').addClass('carouselActive');
		}

		$('.ubirata .age .carouselActive').owlCarousel({
			loop: false,
			navSpeed: 1000,
			slideSpeed: 1000,
			center: false,
			margin: 10,
			responsiveClass: true,
			nav: true,
			dots: false,
			autoWidth: false,
			navContainerClass: 'navGreen owl-nav',
			responsive: {
				0: {
					items: 1,
				},
				800: {
					items: 1,
				},
				1024: {
					items: 4,
				},
				1700: {
					items: 5,
				},
			},
		});

		function varifyActiveFilter() {
			$('body').removeClass(function (index, className) {
				return className
					.split(' ')
					.filter(function (c) {
						return c !== 'ubirata';
					})
					.join(' ');
			});

			if ($('.yellowBackground').hasClass('active')) {
				$('body').addClass('yellowBackground');
			}
			if ($('.blueBackground').hasClass('active')) {
				$('body').addClass('blueBackground');
			}
			if ($('.pinkBackground').hasClass('active')) {
				$('body').addClass('pinkBackground');
			}
			if ($('.easeBackground').hasClass('active')) {
				$('body').addClass('easeBackground');
			}
			if ($('.darkGreenBackground').hasClass('active')) {
				$('body').addClass('darkGreenBackground');
			}
			if ($('.greenBackground').hasClass('active')) {
				$('body').addClass('greenBackground');
			}
		}
	}

	if ($('main').hasClass('mothers')) {
		$('.ubirata .mothers .introduction').owlCarousel({
			loop: false,
			navSpeed: 500,
			center: false,
			rewind: true,
			margin: 20,
			responsiveClass: true,
			nav: true,
			dots: true,
			autoplay: true,
			autoplayTimeout: 3000,
			autoWidth: false,
			navContainerClass: 'navGreen owl-nav',
			responsive: {
				0: {
					items: 1,
					nav: false,
				},
				800: {
					items: 1,
					nav: false,
				},
				1024: {
					items: 1,
				},
				1950: {
					items: 1,
				},
			},
		});

		$('.ubirata .mothers .grid').masonry({
			itemSelector: '.grid-item',
			gutter: 20,
			percentPosition: true,
			stamp: '.newsLetterCard',
		});

		$('.ubirata .mothers .intro a, .ubirata .mothers .introduction a').click(
			function (e) {
				e.preventDefault();

				var target = $(this.hash);

				if ($('main').hasClass('mothers')) {
					$('html, .ubirata').animate(
						{
							scrollTop: target.offset().top - 130,
						},
						800
					);
				}
			}
		);
	}

	if ($('main').hasClass('about')) {
		$('.ubirata .about .introduction a').click(function (e) {
			e.preventDefault();

			var target = $(this.hash);

			$('html, .ubirata').animate(
				{
					scrollTop: target.offset().top - 80,
				},
				800
			);
		});
	}

	if ($('main').hasClass('ambassadorsPage')) {
		$('.ubirata .ambassadorsPage .grid').masonry({
			itemSelector: '.grid-item',
			gutter: 20,
			percentPosition: true,
			stamp: '.newsLetterCard',
		});

		$('.ubirata .ambassadorsPage .open-content').click(function () {
			// Adicione a classe "searchModalActive" ao elemento body
			$('body').addClass('openText');
		});
	}

	if ($('main').hasClass('.searchPage')) {
		$('.ubirata .searchPage .content').owlCarousel({
			loop: false,
			navSpeed: 500,
			center: false,
			margin: 20,
			responsiveClass: true,
			nav: true,
			autoplay: false,
			autoWidth: false,
			navContainerClass: 'navGreen owl-nav',
			responsive: {
				0: {
					items: 1,
				},
				800: {
					items: 2,
				},
				1024: {
					items: 3,
				},
				1530: {
					items: 4,
				},
				1700: {
					items: 5,
				},
			},
		});

		$('.ubirata .searchPage .toolTip').click(function (e) {
			var $contentCard = $(this).closest('.contentCard');
			if ($contentCard.length > 0) {
				$contentCard.toggleClass('modalTipActive');
			}
		});

		$(document).on('click', function (event) {
			if ($('.ubirata .searchPage .contentCard').hasClass('modalTipActive')) {
				// Verifique se o clique não ocorreu nem no elemento com classe 'modalTerm'
				// nem dentro da tag com classe 'toolTip'
				if (
					!$(event.target).closest('.modalTerm').length &&
					!$(event.target).closest('.toolTip').length
				) {
					// Se o clique não ocorrer dentro do modal, oculte o modal
					$('.ubirata .searchPage .contentCard').removeClass('modalTipActive');
				}
			}
		});
	}

	if ($('main').hasClass('politicsAndTerms')) {
		$('.ubirata .politicsAndTerms a').click(function (e) {
			e.preventDefault();

			var target = $(this.hash);
			var targetOffset = target.offset().top - 130;

			if ($('main').hasClass('politicsAndTerms')) {
				$('html, .ubirata').animate(
					{
						scrollTop: targetOffset,
					},
					800,
					function () {
						// Após a animação de rolagem, remove todas as classes "Active" do body
						$('body').removeClass(contentIds.map((id) => id + 'Active').join(' '));
						// Adiciona a classe "Active" com base no ID do elemento de destino
						$('body').addClass(`${target.attr('id')}Active`);
					}
				);
			}
		});

		const contentIds = [];

		// Encontra todos os IDs de conteúdo dinamicamente
		$('h3[id^="content"]').each(function () {
			contentIds.push($(this).attr('id'));
		});

		// Verificação de rolagem para adicionar a classe "active"
		$(window).scroll(function () {
			const scrollTop = $(window).scrollTop();

			for (let i = 0; i < contentIds.length; i++) {
				const contentId = contentIds[i];
				const contentElement = $(`#${contentId}`);
				const targetOffset = contentElement.offset().top - 140;

				if (scrollTop >= targetOffset) {
					// Remove todas as classes "Active" do body
					$('body').removeClass(contentIds.map((id) => id + 'Active').join(' '));
					// Adiciona a classe "Active" com base no ID do elemento de destino
					$('body').addClass(`${contentId}Active`);
				}
			}
		});

		// Verificação inicial quando a página carrega
		$(window).on('load', function () {
			// Remove todas as classes "Active" do body inicialmente
			$('body').removeClass(contentIds.map((id) => id + 'Active').join(' '));
			// Define a classe "Active" com base na posição de rolagem inicial
			addActiveClassOnScroll();
		});

		if ($(window).width() <= 1024) {
			$('.ubirata .politicsAndTerms .menu-sticky').addClass('owl-carousel');
			$('.ubirata .politicsAndTerms .menu-sticky').addClass('owl-theme');
			$('.ubirata .politicsAndTerms .menu-sticky').addClass('carouselPolitics');
			$('.ubirata .politicsAndTerms .carouselPolitics').owlCarousel({
				loop: false,
				navSpeed: 500,
				center: false,
				margin: 0,
				responsiveClass: true,
				nav: true,
				dots: false,
				autoWidth: false,
				navContainerClass: 'navGreen owl-nav',
				responsive: {
					0: {
						items: 1,
					},
					1024: {
						items: 4,
					},
					1700: {
						items: 5,
					},
				},
			});

			const textWrapper = document.querySelector('.text-wrapper');
			const menuWrapper = document.querySelector('.menu-wrapper');
			const firstContent = document.querySelector('.firstContent');

			window.addEventListener('scroll', () => {
				const textBottom = textWrapper.offsetTop + textWrapper.clientHeight;
				const screenHeight = window.innerHeight;
				const menuBottomThreshold = screenHeight - 80; // 70px do fundo da tela

				if (window.scrollY < firstContent.offsetTop + firstContent.clientHeight) {
					menuWrapper.style.display = 'none';
				} else if (!(window.scrollY < textBottom - menuBottomThreshold)) {
					menuWrapper.style.display = 'none';
				} else {
					menuWrapper.style.display = 'block';
				}
			});
		}
	}

	if ($('main').hasClass('my-account')) {
		$('.ubirata .my-account #genderField').SumoSelect({
			search: false,
			// placeholder: 'Selecione seu gênero',
			placeholder: true,
			floatWidth: 0,
		});

		$('.ubirata .my-account #stateField').SumoSelect({
			search: false,
			// placeholder: 'Selecione o seu estado',
			placeholder: true,
			floatWidth: 0,
		});

		$('.ubirata .my-account #cityField').SumoSelect({
			search: false,
			// placeholder: 'Selecione a sua cidade',
			placeholder: true,
			floatWidth: 0,
		});

		$('.ubirata .my-account #statusField').SumoSelect({
			search: false,
			// placeholder: 'Selecione seu status',
			placeholder: true,
			floatWidth: 0,
		});

		$('.ubirata .target-active').click(function () {
			// Adicionar a classe "active" ao elemento pai do item clicado
			$(this).parent().toggleClass('active');
		});

		$('.addMember').click(function () {
			var input = $('#familyField');
			var currentValue = parseInt(input.val());
			input.val(currentValue + 1);
			$('.removeMember').removeClass('disabled'); // Remova a classe "disabled" quando adicionar um membro.
		});

		$('.removeMember').click(function () {
			var input = $('#familyField');
			var currentValue = parseInt(input.val());
			if (currentValue > 0) {
				input.val(currentValue - 1);
			}

			if (currentValue === 1) {
				$('.removeMember').addClass('disabled'); // Adicione a classe "disabled" quando o valor for igual a 1.
			}
		});

		$('.editKidButton').click(function () {
			// Encontre o card pai do botão clicado
			var card = $(this).closest('.kidCard');

			// Encontre o elemento "editKid" dentro do card e adicione a classe "active"
			card.find('.editKid').toggleClass('active');
		});

		$('#phoneNumberField').on('input', function () {
			let inputValue = $(this).val().replace(/\D/g, '');

			if (inputValue.length > 9) {
				inputValue = inputValue.slice(0, 9);
			}

			if (inputValue.length > 5) {
				inputValue = inputValue.slice(0, 5) + '-' + inputValue.slice(5);
			}

			$(this).val(inputValue);
		});

		$('#kidBirthField').on('input', function () {
			console.log('aqui');
			let inputValue = $(this).val().replace(/\D/g, '');

			if (inputValue.length > 8) {
				inputValue = inputValue.slice(0, 8);
			}

			if (inputValue.length > 4) {
				inputValue =
					inputValue.slice(0, 2) +
					'/' +
					inputValue.slice(2, 4) +
					'/' +
					inputValue.slice(4, 8);
			} else if (inputValue.length > 2) {
				inputValue = inputValue.slice(0, 2) + '/' + inputValue.slice(2, 4);
			}

			$(this).val(inputValue);
		});

		$('#cepField').on('input', function () {
			let cep = $(this).val().replace(/\D/g, '');

			if (cep.length > 8) {
				cep = cep.slice(0, 8);
			}

			if (cep.length > 5) {
				cep = cep.replace(/(\d{5})(\d{3})/, '$1-$2');
			}

			// Defina o valor do campo como uma string
			this.value = cep;
		});
	}

	if ($('main').hasClass('my-content')) {
		$('.ubirata .my-content .grid').masonry({
			itemSelector: '.grid-item',
			gutter: 20,
			percentPosition: true,
			stamp: '.newsLetterCard',
		});

		$('.ubirata .my-content .postOrder').SumoSelect({
			search: false,
			placeholder: 'Selecione as tags do tema',
			floatWidth: 0,
		});

		// FILTRO

		$('.ubirata .my-content .filter .item').on('click', function () {
			// Remove a classe "active" de todas as divs dentro de .filter
			$('.filter .item').removeClass('active');
			// Adiciona a classe "active" à div clicada
			$(this).addClass('active');
			// Remove a classe "active" de todas as divs com texto "developmentPhase"
			$('div:contains("developmentPhase")').removeClass('active');
			// Adiciona a classe "active" à div clicada

			if ($(this).hasClass('myKids')) {
				activateElementByClass('phase1');
			}
			// Verifica se o elemento clicado tem a classe "kid" seguida de "phase1" e ativa phase2
			else if ($(this).hasClass('kid') && $(this).hasClass('1')) {
				activateElementByClass('phase2');
			}
			// Verifica se o elemento clicado tem a classe "kid" seguida de "phase2" e ativa phase3
			else if ($(this).hasClass('kid') && $(this).hasClass('2')) {
				activateElementByClass('phase3');
			}
			// Verifica se o elemento clicado tem a classe "read" e ativa phase4
			else if ($(this).hasClass('read')) {
				activateElementByClass('phase4');
			}
			// Verifica se o elemento clicado tem a classe "saved" e ativa phase5
			else if ($(this).hasClass('saved')) {
				activateElementByClass('phase5');
			}

			$('.ubirata .grid').masonry('layout');
		});

		// Função para ativar elementos com base na classe do elemento clicado
		function activateElementByClass(elementClass) {
			// Remove a classe "active" de todos os elementos de desenvolvimento
			$('.developmentPhase').removeClass('active');
			// Adiciona a classe "active" ao elemento de destino com base na classe do elemento clicado
			$(`.${elementClass}`).addClass('active');
		}

		$('.ubirata .my-content .filter.carouselFilter').owlCarousel({
			loop: false,
			navSpeed: 1000,
			slideSpeed: 1000,
			center: false,
			margin: 10,
			responsiveClass: true,
			nav: true,
			dots: false,
			autoWidth: false,
			navContainerClass: 'navGreen owl-nav',
			responsive: {
				0: {
					items: 1,
				},
				800: {
					items: 1,
				},
				1024: {
					items: 4,
				},
				1200: {
					items: 5,
				},
				1530: {
					items: 6,
				},
				1950: {
					items: 6,
				},
			},
		});
	}
});
